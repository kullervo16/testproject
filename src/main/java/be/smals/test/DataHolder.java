/*
 * Copyright (c) Smals
 */
package be.smals.test;


/**
 * TODO: Description of the class.
 * 
 * @author jeve
 * 
 * @since 
 * 
 */
public class DataHolder {
    private Object data;
    private int counter;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataHolder other = (DataHolder) obj;
        if (this.data != other.data && (this.data == null || !this.data.equals(other.data))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.data != null ? this.data.hashCode() : 0);
        return hash;
    }
        

    @Override
    public String toString() {
        return "DataHolder{" + "data=" + data + ", counter=" + counter + '}';
    }
        

}
