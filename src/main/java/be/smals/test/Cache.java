/*
 * Copyright (c) Smals
 */
package be.smals.test;


/**
 * TODO: Description of the class.
 * 
 * @author jeve
 * 
 * @since 
 * 
 */
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author jeve
 */
public class Cache {
    
    private final HashMap<String, DataHolder> cache = new HashMap<String, DataHolder>();
    
    public Object getElement(String key) {
        synchronized (cache) {
            if(cache.containsKey(key)) {
                DataHolder holder = cache.get(key);
                holder.setCounter(holder.getCounter() + 1);
                return holder.getData();
            }
            throw new IllegalStateException("Element with "+key+" not found");
        }
    }
    
    public void setElement(String key, Object value) {
        synchronized (cache) {
            if(cache.containsKey(key)) {
                DataHolder holder = cache.get(key);
                if(holder.getData().equals(value)) {
                    // already in
                    return;
                }
            } 
            DataHolder holder = new DataHolder();
            holder.setData(value);            
            holder.setCounter(0);
            cache.put(key, holder);
        }
    }
        
    public void purge() {
        HashMap<String, DataHolder> copy;
        synchronized (cache) {
            copy = cache;
        }
            
        Iterator<DataHolder> it = copy.values().iterator();
        while(it.hasNext()) {
            if(it.next().getCounter() < 3) {
                it.remove();
            }
        }

    }

    @Override
    public String toString() {
        return "Cache{" + "cache=" + cache + '}';
    }
        
}
