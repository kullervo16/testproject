/*
 * Copyright (c) Smals
 */
package be.smals.test;


/**
 * TODO: Description of the class.
 * 
 * @author jeve
 * 
 * @since 
 * 
 */
public class MyBeautifulObject {
    private String value;

    public MyBeautifulObject(String value) {
        this.value = value;
    }
        

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "MyBeautifulObject{" + "value=" + value + '}';
    }
}
