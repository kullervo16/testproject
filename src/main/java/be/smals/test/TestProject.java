/*
 * Copyright (c) Smals
 */
package be.smals.test;


/**
 * TODO: Description of the class.
 * 
 * @author jeve
 * 
 * @since 
 * 
 */
public class TestProject {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Cache c = new Cache();
        c.setElement("boe", new MyBeautifulObject("Test"));
        c.setElement("baa", new MyBeautifulObject("Miep"));
        c.setElement("jihaa", Double.MAX_VALUE);        
         
        System.out.println(c);
        
        c.getElement("boe");
        c.getElement("boe");
        System.out.println(c);
        
        c.setElement("boe", new MyBeautifulObject("Test"));
        c.getElement("boe");        
        
        c.purge();
        
        System.out.println(c);
    }
}
